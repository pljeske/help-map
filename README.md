For a quick start you can use Docker to deploy the application

1. After cloning the git repository build the Docker container with:

    docker build -t helpmap .


2. To run the application, listening on localhost:80 use:

    docker run -itd -p 80:3013 helpmap