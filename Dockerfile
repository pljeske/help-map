FROM python:latest
ENV PYTHONUNBUFFERED 1

USER root

RUN apt-get update
RUN apt-get install sqlite3

RUN mkdir /src
WORKDIR /src
ADD . /src

RUN pip install -r requirements.txt

RUN python manage.py migrate

CMD gunicorn CoronaHelpMap3.wsgi -b 0.0.0.0:3013

EXPOSE 3013